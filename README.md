# JUnit Demo with CI

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/noroff-accelerate/java/projects/junit-ci-demo/badges/master/pipeline.svg)](https://gitlab.com/noroff-accelerate/java/projects/junit-ci-demo/-/commits/master)

Demo project with automatic unit tests using Gradle and JUnit

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Usage

For Linux/macOS users, open a terminal and run:

```sh
./gradlew build
cd build/classes/java/main
java Main
```

For Windows users, use `gradlew.bat` instead of `gradlew` in PowerShell.

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Noroff Accelerate AS
