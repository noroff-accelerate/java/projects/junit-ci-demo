public class ElectricGuitar extends Guitar {
    private String pickupType;

    public ElectricGuitar(String brand, String model, int year, String pickupType) {
        super(brand, model, year);
        this.setPickupType(pickupType);
    }

    @Override
    public String makeSound() {
        return String.format("electric guitar sounds with %s pickup", this.pickupType);
    }

    public String getPickupType() {
        return pickupType;
    }

    public void setPickupType(String pickupType) {
        this.pickupType = pickupType;
    }
}
