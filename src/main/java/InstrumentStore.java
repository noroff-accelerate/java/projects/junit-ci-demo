import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InstrumentStore {
    private List<Playable> instruments;

    public InstrumentStore(Playable[] instruments) {
        this.instruments = new ArrayList(Arrays.asList(instruments));
    }

    public List<Playable> getInstruments() {
        return instruments;
    }

    public void playInstruments() {
        for (Playable p : instruments) {
            System.out.println(p.makeSound());
        }
    }
}
