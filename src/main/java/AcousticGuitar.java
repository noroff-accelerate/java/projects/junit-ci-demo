public class AcousticGuitar extends Guitar {
    private int numberOfStrings;

    public AcousticGuitar(String brand, String model, int year, int numberOfStrings) {
        super(brand, model, year);
        this.setNumberOfStrings(numberOfStrings);
    }

    @Override
    public String makeSound() {
        return String.format("acoustic guitar sounds with %d string", this.numberOfStrings);
    }

    public int getNumberOfStrings() {
        return numberOfStrings;
    }

    public void setNumberOfStrings(int numberOfStrings) {
        this.numberOfStrings = numberOfStrings;
    }
}
