public abstract class Guitar implements Playable {
    private String brand;
    private String model;
    private int yearManufactured;

    public Guitar(String brand, String model, int year) {
        setBrand(brand);
        setModel(model);
        setYearManufactured(year);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYearManufactured() {
        return yearManufactured;
    }

    public void setYearManufactured(int yearManufactured) {
        this.yearManufactured = yearManufactured;
    }
}
