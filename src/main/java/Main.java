public class Main {
    public static void main (String[] args) {
        InstrumentStore store = new InstrumentStore(new Playable[]{
                new AcousticGuitar("foo", "bar", 2022, 6),
                new ElectricGuitar("baz", "qux", 2022, "standard"),
                new Triangle()
        });

        store.playInstruments();
    }
}
