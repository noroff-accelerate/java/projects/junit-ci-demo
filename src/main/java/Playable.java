public interface Playable {
    public String makeSound();
}
