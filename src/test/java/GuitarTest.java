import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GuitarTest {
    Guitar guitar;
    static String originalBrand = "demo";
    static String originalModel = "mk2";
    static int originalYear = 2022;
    static String updatedBrand = "demo2";
    static String updatedModel = "mk3";
    static int updatedYear = 2023;
    static String baseGuitarSound = "generic guitar sounds";

    @BeforeEach
    void setUp() {
        guitar = new Guitar(originalBrand, originalModel, originalYear) {
            @Override
            public String makeSound() {
                return baseGuitarSound;
            }
        };
    }

    @Test
    void setBrand_ValidValues_ShouldPass() {
        guitar.setBrand(updatedBrand);
        Assertions.assertEquals(guitar.getBrand(), updatedBrand);
    }

    @Test
    void setModel_ValidValues_ShouldPass() {
        guitar.setModel(updatedModel);
        Assertions.assertEquals(guitar.getModel(), updatedModel);
    }

    @Test
    void setYearManufactured_ValidValues_ShouldPass() {
        guitar.setYearManufactured(updatedYear);
        Assertions.assertEquals(guitar.getYearManufactured(), updatedYear);
    }

    @Test
    void getBrand_ValidValues_ShouldPass() {
        Assertions.assertEquals(guitar.getBrand(), originalBrand);
    }

    @Test
    void getModel_ValidValues_ShouldPass() {
        Assertions.assertEquals(guitar.getModel(), originalModel);
    }

    @Test
    void getYearManufactured_ValidValues_ShouldPass() {
        Assertions.assertEquals(guitar.getYearManufactured(), originalYear);
    }

    @Test
    void makeSound_OverridenValues_ShouldPass() {
        Assertions.assertDoesNotThrow(() -> {
            Assertions.assertEquals(guitar.makeSound(), baseGuitarSound);
        });
    }
}