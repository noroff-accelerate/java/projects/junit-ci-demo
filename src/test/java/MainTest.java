import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void main() {
        Assertions.assertDoesNotThrow(() -> Main.main(new String[] {}));
    }
}